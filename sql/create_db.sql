create database biblio;
use biblio;
create table entries (author text, title text, year int, journal text);